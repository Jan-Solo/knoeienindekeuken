---
hero:
  heading: "~ KNOEIEN IN DE KEUKEN ~"
  subheading:  Jan en Femke's zoektocht naar de lekkerste receptjes
  maxWidthPX: 652
seo:
  image: /images/favicon.svg
---
