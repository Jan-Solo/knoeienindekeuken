+++
CookingTime = 60
authors = [ "Jan en Femke" ]
date = 2020-10-15T20:07:38Z
exerpt = "Wil je eens iets anders dan die saaie patat? Probeer dan zeker eens dit lekker, Mexicaans pakje geluk uit. "
hero = "/uploads/img_20201015_222617.jpg"
title = "Mexicaanse burrito's"

+++

# Voor 4 personen 

## Ingrediënten

* Wraps 
* 800 gram kalfs- en runds gehakt 
* 3 vleestomaten
* 2 groene paprika's 
* 100 witte bonen 
* 150 gram zwarte bonen
* 120 gram mais 
* 1 rode peper 
* 2 teentjes knoflook 
* Peper 
* Zout 
* Chilivlokken 
* Zure zoom
* Gemalen kaas 
* Bakboter 

## Bereidingswijze 

 1. Verwarm de over voor op 180 graden Celcius.
 2. Doe een scheut bakboter in een grote, goed verhitte pan. 
 3. Doe het vlees hierin en breng op smaak met zout, peper en chillivlokken. 
 4. Verwijder te pitjes uit de rode paprika en hak deze samen met de knoflook fijn. Voeg vervolgens toe aan het vlees en meng het goed eronder. 
 5. Verwijder de pitjes uit de groene paprika en snijd in stukjes. Voeg toe aan het vlees en meng. Doe hetzelfde met de vleestomaten. 
 6. Voeg vervolgens alle geblikte materialen toe na het goed laten uitlekken. 
 7. Meng alles goed onder elkaar en laat op een middelhoog vuur 10 minuten verder pruttelen. Roer regelmatig. 
 8. Smeer een goede laag zure room op wraps. Schep daarna een poritie naar keuze van het groenten-en-vlees-mengsel op de wrap. Zie wel dat je de wrap nog kan toedoen. Voor je de wrap toe doe voeg je nog wat gemalen kaas toe. Leg de burrito in de ovenschaal en herhaal dit proces. 
 9. Zet de burrito's in de oven en laat ze bakken tot ze bruiniger worden en krokant lijken. 
10. Serveer warm. 

Wij raden aan hierbij guacamole te maken als een soort van dip. Onderstaand kan je onze variant terugvinden. 

## Ingrediënten guacamole 

* 3 rijpe advocado's 
* 2 kleine tomaten 
* 1 limoen 
* Zout 
* Peper 
* Chillivlokken 

Guacamole is ontzettend gemakkelijk om te maken. Je moet gewoon de advocado's ontpitten. Laat 1 pit in het mengsel zodat het langer lekker blijft. Breng het op smaak met het limoensap, zout, peper en de chillivlokken. Voeg de tomaat in kleine stukjes gesneden toe en meng goed. 