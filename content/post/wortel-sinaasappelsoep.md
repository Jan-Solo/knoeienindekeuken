+++
CookingTime = 30
authors = [ "Jan en Femke" ]
date = 2022-04-20T11:59:01Z
draft = false
exerpt = "Verassend fris en origineel zonder traditionele wortelsoep te verraden"
hero = "/uploads/wortelsinaasappelsoep.jpg"
title = "Wortel sinaasappelsoep"

+++
# VOOR VIER TOT ZES PERSONEN 

## Benodigheden 

* 5 grote wortels
* 1 sinaasappel
* 1 limoen
* 1 blokje gember
* 1 teentje knoflook
* Basilicumcreme
* Olijfolie
* 4 groentebouillonblokjes
* 125 ml witte wijn (liefst zoete)
* 1l water

## Bereidingswijze 

1. Maak de wortel schoonen en rasp een halve wortel redelijk grof en snij de rest van de wortels in plakjes.
2. Pel de knoflook en hak fijn.
3. Schil de gember en hak fijn.
4. Pers de sinaasappel en limoen **(sappen apart houden)**
5. Verhit wat olijfolie in je kookpan en bak de knooflook en gember een minuut of 2.
6. Voeg de plakjes wortel, witte wijn, sinaasappelsap, water en bouillonblokjes toe en laat 20 minuten koken.
7. Mixen tot er geen brokken meer in zitten. _mix it up_
8. Voeg de geraspte wortel en limoensap toe.
9. Men de basilicumcreme met wat olijfolie.
10.  Serveer

