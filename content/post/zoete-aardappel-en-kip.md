+++
CookingTime = 90
authors = [ "Jan en Femke"]
date = 2020-12-09T20:25:17Z
exerpt = "Wil je een kleurijke en vooral lekker explosie op je bord hebben? Dan it dit het gerecht voor jou. "
hero = "/uploads/img_20201129_123141.jpg"
title = "Zoete aardappel en kip"

+++
# Voor vier personen 

## Benodigdheden 

* 2 middelgrote zoete aardappels 
* 600 gram kip 
* 2 grote rode paprika's 
* 500 gram princessboontjes 
* 8 grote wortels 
* Zout en peper 
* Chilivlokken 
* Kipkruiden
* Olijfolie 
* Fleur de sel 
* Eventueel sas naar keuze 

## Bereidingswijze 

1. Verwarm de oven voor op 180 graden. 
2. Was de groenten (boontjes, paprika, wortel en zoete aardappel). Snijd de kontjes van de bonen. Verwijder de pitjes uit de paprika en snijd in dunne schijfjes. Schil de wortel en Snijd in schijfjes. Schil de zoete aardappel en snijd in blokjes. 
3. Vul een kookpot met wat voor de zoete aardappel en een kookpot voor de boontjes en wortels. Zet op hoog vuur en breng aan de kook en breng het water op smaak met zout en peper. Laat ze niet volledig gaar koken. 
4. Laat de wortels, boontjes en zoete aardappel goed uitlekken en leg ze samen met de paprika op een ovenplaats. Giet er een beetje olijfolie over en breng op smaak met de fleur de sel. 
5. Snijd de knip in kleine blokjes en breng op smaak met de kipkruiden, chilivlokken, zout en peper. Meng de kruiden goed met de kip. 
6. Bak de kip in een beetje olijfolie. 
7. Maak je bord en geniet van een lekker gerecht met eventueel een saus naar smaak. 
