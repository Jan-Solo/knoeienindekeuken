+++
CookingTime = 30
authors = ["Jan en Femke"]
date = 2022-11-22T21:50:32Z
exerpt = "Wil je eens iets wat een beetje anders is maar toch snel klaar? Probeer deze zeker deze pittige curry eens. Je gaat het je niet beklagen! "
hero = "/uploads/pxl_20221006_155535433.jpg"
title = "Curry time "

+++
# VOOR 4 PERSONEN 

### INGREDIËNTEN 

* 500 gram kipfilet 
* 1 bloemkool 
* 3 rode paprika's 
* 250ml kokosmelk 
* 2  chilipepers
* Gele currypasta 
* Peper 
* Zout 
* Olijfolie
* Kippenkruiden
* 300 gram basmati rijst 

### BEREIDINGSWIJZE 

1. Spoel de bloemkool en paprika's af. Snijd de paprika's in reepjes en de bloemkool in roosjes. Laat de bloemkool koken voor ongeveer 5 minuten en gezouten water. 
2. Snijd de kipfilet in stukjes naar keuze, maar snijd ze wel even groot. Kruid de kip met kippenkruiden en de fijngehakte chilipepers. Bak de kip in olijfolie, maar nog niet volledig door. 
3. Gooi de bloemkool, paprika en kip samen in een diepe pan en laat even sudderen. 
4. Voeg vervolgens de kokosmelk en gele currypasta toe. Meng alles goed door elkaar. 
5. Laat het geheel een 10 minuten op een middelhoog tot laag vuur sudderen. Roer regelmatig. 
6. Kook gelijktijdig de rijst zoals aangegeven op de instructies. Maar was de rijst voor je ze kookt. 

_TIP!_

De  rijst is pas gewassen als het water geen witte schijn meer heeft. 