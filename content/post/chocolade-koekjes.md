+++
CookingTime = 50
authors = ["Jan en Femke"]
date = 2020-08-17T14:36:50Z
exerpt = "Het is leuk om te maken, maar nog beter om te eten. Vergeet even dat gezonde eten en zondig met deze gemakkelijk te maken koeken. "
hero = "/uploads/img_20200815_164634.jpg"
title = "Chocolade koekjes"

+++
## Ingrediënten

* 200 gram melkchocolade
* 200 gram witte chocolade
* 1 groot ei
* 200 gram patisserieboter
* 325 gram zelfrijzende bloem
* 150  gram suiker
* Eventueel melk

## Bereidingswijze

1. Verwarm de oven voor op 180 graden.
2. Weeg de suiker af en voeg de op kamertemperatuur patisserieboter erbij. Meng dit goed.
3. Voeg het ei toe en meng.
4. Weeg de zelfsrijzende bloem af en voeg deze geleidelijke toe aan het mengsel.
5. Indien het deeg wat droog is, voeg een scheut melk toe.
6. Snijd de witte en melkchocolade in kleine stukken en voeg toe aan het mengsel. Zorg dat de chocolade even verdeeld.
7. Verdeel het deeg in balletjes, hoe groter hoe langer ze moeten bakken.
8. Leg de balletjes per 4 op een met bakpapier bedekte bakplaat en laat ze bakken voor 10 minuten. Afhankelijk van de grootte kunnen ze sneller of minder snel bakken. Houd ze dus in het oog.
9. Laat ze afkoelen en genieten maar.

Give credit where credit is due, dit is niet ons recept, maar het recept van Tanya Burr. 