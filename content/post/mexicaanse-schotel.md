+++
CookingTime = 40
authors = ["Jan en Femke"]
date = 2023-04-06T10:53:05Z
exerpt = "Ovenschotels zijn altijd lekker, maar deze is toch iets anders dan de typische Belgische ovenschotel. Want ja, afwisseling is altijd leuk!"
hero = "/uploads/pxl_20230124_175334687.jpg"
title = "Mexicaanse schotel"

+++
# 4/6 PERSONEN 

## Ingrediënten 

* 500gr rundsgehakt (ander gehakt ook oké) 
* 2 middelgrote zoete aardappels 
* 2 grote paprika's 
* 400gr kidney bonen 
* 150gr mais 
* 1 fajita kruidenmix (vindbaar in albert heijn) 
* 1 rode peper 
*  een handje gemalen kaas 
* Peper 
* Zout 

## Bereidingswijze 

1. Verwarm de oven voor op 180°C.
2. Was de paprika's en snijd in stukken. Schil de zoete aardappel en snij in blokjes. Laat de andere groenten (bonen en mais) leeglopen. 
3. Meng de fajita kruidenmix onder de zoete aardappel. 
4. Snijd de rode peper en voeg aan het gehakt toe, samen met peper en zout. Bak het gehakt nog niet volledig, het gaart verder door in de oven. 
5. Doe het gehakt op de bodem van de ovenschaal. Meng alle andere ingrediënten en voeg toe aan de ovenschaal. 
6. Zet de ovenschaal voor 20minuten in de oven. Afhankelijk van hoe knapperig de groenten moeten zijn iets langer of minder lang. Let vooral op de aardappel, je moet hierin kunnen prikken.
7. Vlak voor de ovenschotel klaar is, een kleine laag kaas over de ovenschotel strooien. 
8. Als de kaas wat gesmolten is, is de schotel serveer klaar. 
9. Smullen maar. 