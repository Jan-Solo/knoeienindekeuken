+++
CookingTime = 40
authors = ["Jan en Femke"]
date = 2020-07-29T18:19:24Z
exerpt = "Heerlijk romige aardappelsoep met speksmaak"
hero = ""
title = "Aardappel- en speksoep"

+++
> "Neem een ketel die voldoende groot is, anders kom je zoals mij in de problemen"

## Ingrediënten

* 1 kilogram aardappelen, geschild en in blokjes gesneden
* 6 reepjes spek, in kleine stukjes gesneden
* 3 eetlepels boter
* 40 gram bloem
* 3 teentjes knoflook, gehakt
* 1 liter kippenbouillon
* 0,5 liter melk
* 150 mililiter kookroom
* peper en zout
* 150 gram zure room

## Bereiding

1. Bak de spekblokjes in de soepketel tot ze lekker knapperig zijn. Als ze klaar zijn leg je ze appart en laat je het vet in de ketel staan.
2. Doe de extra boter in de pan en stoof de knoflook gedurende 30 seconden.
3. Doe nu de aardappelen, kippenbouillon, kookroom en melk in de ketel, vergeet niet alles goed door elkaar te roeren.
4. Peper en zout toevoegen naar smaak.
5. Breng het gehaal aan de kook tot de aardappelen gaar zijn.
6. Mix alles tot een zachte soep
7. Voeg de spekjes toe samen met de zure room (tenzij je de soep wil invriezen).

## Bonus tips

Liefhebbers kunnen de soep afwerken met Cheddar kaas of bieslook