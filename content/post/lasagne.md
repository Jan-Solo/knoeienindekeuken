+++
CookingTime = 60
authors = ["Jan en Femke"]
date = 2020-08-03T15:05:13Z
exerpt = "Heb je eens wat tijd en wil je samen koken? Dan is deze lasange het ideale gerecht om te maken."
hero = "/uploads/lasange.png"
title = "Lasagne "

+++
# Voor 6 personen 

## Ingrediënten 

* 1 grote courgette 
* 5 wortels 
* 1 grote vleestomaat 
* 1 fles pasata 
* 400 gram gemalen kaas 
* 750 gram gemengd gemalen 
* Melk
* Lasagne vellen 
* Provençaalse kruiden 
* 2 teentjes look
* Oregano 
* Basilicumkruiden 
* 45 gram boter 
* 45 gram bloem 
* Peper 
* Zout 
* Olijfolie

## Bereidingswijze 

Voor de tomatensaus 

1. Was en snijd de courgette, wortels en tomaat vergeet de wortels en tomaat niet te schillen. 
2. Zet een grote kookpot op het vuur en gooi hier een scheut olijfolie in. Voeg later het gemalen toe en zorg dat het gelijk bakt. 
3. Versnipper de look en voeg toe. 
4. Breng het gemalen op smaak door de provençaalse kruiden, oregano, basilicumkruiden, peper en zout toe te voegen. 
5. Verlaag het vuur en voeg de groenten toe. Laat alles even sudderen. 
6. Als het gemalen goed gebakken is voeg je de pasata toe. Roer goed onder het vlees en de groenten in. 
7. Indien de saus niet dik genoeg is meng je in een fles wat melk en bloem. Voeg deze geleidelijk toe, tot je de dikheid bereikt die je verkiest. 
8. Haal van het vuur als het klaar. 

Voor de kaassaus 

1. Zet een kookpot op het vuur en laat de boter warm worden. Zorg ervoor dat de boter volledig gesmolten is. 
2. Voeg de bloem in één keer toe en goed roeren. Blijft constant roeren.
3. Voeg geleidelijk aan de melk toe tot je de juiste dikte bereikt. 
4. Blijf goed roeren terwijl je de gemalen kaas erbij doet. 
5. Breng op smaak met zout en peper. 

De afwerking 

1. Verwarm de overn voor op 180 graden. 
2. Smeer een dun laagje boter op de bodem van de ovenschotel, zo kan je deze gemakkelijk eruit halen. 
3. Begin met een laag van de tomatensaus en verdeel deze gelijk. 
4. Leg een eerste laag van de lasagne vellen. Zorg ervoor dat deze niet over elkaar liggen, zo voorkom je een dikke pastalaag.
5.  Vervolgens een dunne laag van de kaassaus. Herhaal dit proces totdat de ovenschotel gevuld is. 
6. Werk af met een laag gemalen kaas. 
7. Zet de lasagne in de oven voor ongeveer 30 minuten. 
8. Serveer de lasagne met een broodje.