+++
CookingTime = 15
authors = [ "Jan en Femke" ]
date = 2021-09-29T11:59:01Z
draft = false
exerpt = "Lekker en gezond, maar vooral gemakkelijk om te maken. Wat wil je nog meer? "
hero = "/uploads/img_20210714_122355.jpg"
title = "Banenenpannekoekjes"

+++
# VOOR ÉÉN/TWEE PERSONEN 

## Benodigheden 

* 2 grote eieren 
* 1 of 2 banenen 
* Bloem 
* Kaneel 

## Bereidingswijze 

1. Pel de banaan of banenen en snijd deze in stukken. Plet de banenen vervolgens met een vork. Er mogen nog stukjes in zitten, deze zorgen voor structuur in de pannenkoek. 
2. Klop de eieren los en voeg ze toe aan de geplette bananen. Meng de twee ingrediënten onder elkaar. 
3. Voeg kaneel naar smaak toe en meng. 
4. De bloem voeg je best geleidelijk aan toe. De bloem bepaalt de dikte van de pannekoek, hoe dikker je de pannenkoek wil hoe meer bloem je nodig hebt. 
5. Vet de pan eenmaal in met olie of boter. Maak de pannekoeken niet te groot. Een kleine pollepel beslag is voldoende. Laat de pannenkoek bakken tot je het vanzelf los kan schudden en draag dan pas om. 

**TIP** 

Deze pannenkoeken zijn enorm lekker met kaneel suiker. Voeg gewoon een beetje kaneel aan de gekozen suiker toe, kristalsuiker werkt het beste, en gebruik als topping!