+++
CookingTime = 35
authors = ["Jan en Femke"]
date = 2022-07-28T20:37:34Z
exerpt = "Eens zin in iets anders? Dan is dit recept zeker iets voor jou. Het is krokant vanuit de oven, maar heeft vooral iets verfrissend door de dressing!"
hero = "/uploads/knoeien.jpg"
title = "Flatbread special"

+++
# Voor 4 flatbreads 

## Ingrediënten 

* Flatbread/naan
* kerstomaten 
* 2 gele paprika's 
* 1 conserve kidney beans 
* 250 gram champignons
* Gehakt of een vegetarische variant 
* 300 gram geconcentreerde tomatenpuree
* 1/2 teentjes look 
* 1 grote citroen 
* Magere yoghurt 
* Honing 
* zout 
* Peper 
* Basilicum kruiden

## Bereidingswijze 

1. Verwarm de oven voor op 180° hetelucht. 
2. Spoel en snijd de groenten in blokjes. 
3. Kruid het vlees of de vegetarische variant en bak totdat het bijna gaar is. Het vlees gaat nog even verder garen in de oven. 
4. Voeg tijdens het bakken van het vlees de paprika, champignons en bonen geleidelijk toe.
5. Doe de geconcentreerde tomatenpuree in een kom en leng aan met water totdat het smeerbaarder is. Hak de knoflook in fijne stukjes en roer eronder samen met zout, peper en basilicum.
6. Doe een laag tomatenpuree op het flatbread/naan en vervolgens een laag van de vlees en groentenmix. Snijd de kerstomaten en verdeel deze over de flatbreads. 
7. Steek de flatbreads in de oven en bak voor 10 minuten, mag iets langer als je krokanter wil, maar let er dan voldoende goed op. 
8. Voor de dressing: Meng de magere yoghurt met honing naar keuze. Dit is afhankelijk van hoe zout je de dressing wil. Raps de citroen, doe zowel de citroenraps als voldoende citroensap in de kom en meng voldoende goed. 
9. Serveer door de dressing erover te doen en smakelijk! 

#### _TIP!_ 

Een volledig broodje kan eventueel zwaar bevallen, vandaar dat een slaatjes erbij serveren zeker een optie is. 
