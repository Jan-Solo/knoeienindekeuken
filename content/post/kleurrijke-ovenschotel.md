+++
CookingTime = 50
authors = [ "Jan en Femke" ]
date = 2020-11-06T11:07:14Z
exerpt = "Nu kan je niet alleen genieten van een lekker gerecht, maar is ook aan de uitspraak het oog wil ook wat voldaan. "
hero = "/uploads/img_20200729_150942.jpg"
title = "Kleurrijke ovenschotel"

+++
# Voor 4 personen 

## Ingrediënten 

* 1 zakje krieltjes gekruid 
* 1 stam brocolli 
* 1 klein bakje campignons 
* 3 middelgrote wortels 
* 500 gram kippenblokjes 
* Geraspte kaas (optioneel) 
* Peper 
* Zout 
* Chilivlokken 
* Kipkruiden 

## Bereidingswijze 

1. Verwarm de over voor op 180 graden. 
2. Spoel en snijd de wortels, brocolli en champignons
3. Kook de brocolli, maar laat het niet volledig doorkoken. Neem na 5 minuten van het vuur en giet uit. 
4. Doe de krieltjes in een pan en laat ze bakken. Ook de krieltjes moeten niet volledig doorbakken zijn. Roer regelmatig. 
5. Bak de wortel in een pak. Kruid met peper en zout. Voeg vervolgens de champignons toe. 
6. Bak de kip en kruid deze met zout, peper, kipkruiden en chilivlokken. Bak ook de kip niet volledig. 
7. Doe alle ingrediënten verdeeld in de ingevette ovenschaal. Voeg eventueel een laagje geraspte kaas toe. 
8. Zet voor 15 à 20 minuten in de oven. 
