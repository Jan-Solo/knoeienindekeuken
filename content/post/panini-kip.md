+++
CookingTime = 15
authors = ["Jan en Femke"]
date = 2020-11-03T14:12:31Z
exerpt = "Zin in een snelle, lekkere hap? Deze panini kip gaat je zeker bevredigen!"
hero = "/uploads/paninikip.png"
title = "Panini kip"

+++
# Voor 4 personen 

## Ingrediënten 

* 4 paninibroodjes 
* 300 gram pulled chicken 
* 1 zakje gemalen kaas 
* 1 grote tomaat 
* Saus naar keuze optioneel 

## Bereidingswijze 

Veel bereiden is er eigenlijk niet aan, je laat je contact grill opwarmen en begint met het maken van de broodjes. Snijd het paninibroodje open en doen de pulled chicken, gemalen kaas en tomaat ertussen. 

Leg het broodje tussen de contact grill, deze zal je panini goed op elkaar drukken. Haal de panini eruit als er lichte grill marks zijn en de kaas goed gesmolten is. 

**TIP**

Panini's zijn gemakkelijk te maken, maar hebben een nog groter voordeel. Ze zijn geweldig om de koelkast mee leeg te maken. Je kan zelf combinaties maken met wat je nog hebt. 

Je moet ook niet perse gebruik maken van paninibroodjes, ook gewoon brood kan ertussen gestoken worden! 

Geniet van je lekkere en originele panini's!
