+++
CookingTime = 45
authors = [ "Jan en Femke" ]
date = 2020-08-10T08:07:17Z
exerpt = "Granola kan je letterlijk altijd eten. Start je dag ermee of maak iets lekkers voor lunch. Wat het nog beter gaat maken is zelfgemaakte granola. "
hero = "/uploads/img_20200806_142830-1.jpg"
title = "Zelfgemaakte granola "

+++
## Ingrediënten 

* 120 gram havermout 
* Kaneel 
* Honing
* Rozijnen 
* 70 gram walnoten 
* 50 gram pompoenpitten 
* 50 gram amandelen 
* 50 gram hazelnoten 
* Eventuele toppings: kokos schilfers, bananenchips,... 

## Bereidingswijze 

 1. Verwarm de oven voor op 180 graden. 
 2. Was de noten en dep goed droog. 
 3. Doe de havermout in een kom en voeg kaneel naar smaak toe. Als je geen fan bent van kaneel kan je deze weglaten. 
 4. Voeg vervolgens honing naar smaak toe en meng goed onder de havermout. Zorg ervoor dat de havermout wat aan elkaar plakt. 
 5. Snijd de walnoten, amandelen en hazelnoten. Snijd de walnoten redelijk klein, laat de amandelen en hazelnoten wat groter. 
 6. Voeg de pompoenpitten, amandelen, hazelnoten en walnoten samen en meng onderling. 
 7. Neem een grote bakplaat en leg er bakpapier op. Verspreid de havermout over de hele bakplaat. 
 8. Verdeel de notenmengeling over de bakplaat. 
 9. Bak de granola ongeveer 30 minuten, maar roer om de 10 minuten. 
10. Voeg achteraf naar smaak rozijnen toe. Voeg eventueel nog andere toppings toe. 

**Opmerking** 

Met granola kan je diverse gerechtjes maken. Je kan het in je smoothiebowls doen of gewoon met yoghurt en fruit. Eerder hebben we een variantje gepost, mocht je inspiratie op willen doen! Maar de mogelijkheden zijn eindeloos!
