+++
CookingTime = 10
authors = ["Jan en Femke"]
date = 2020-08-01T11:42:07Z
exerpt = "Het oogt mooi, maar smaakt nog beter. Het is een een fris gerechtje dat je zowel als ontbijt, lunch of snack kan eten. Je kan het snel bij elkaar gooien en het zal je buikje goed vullen met een lekkere portie yoghurt, fruit en zelfgemaakte granola."
hero = "/uploads/img_20200801_125557.jpg"
title = "Fruitige yoghurt met granola"

+++
# Voor 1 persoon

## Ingrediënten

* 75 gram magere yoghurt
* 3 à 4 eetlepels granola
* 1/2 perzik
* 2 eetlepels granaatappel
* Honing

## Bereidingswijze

1. Pak een kleine kom en doe daar een een bodem magere yoghurt in.
2. Verspreid de  granola over de bodem magere yoghurt.
3. Doe de resterend magere yoghurt aan één kan van het kommetje.
4. Was de perzik en snijd deze in twee. Snijd één helft van de perzik in kleine stukjes en verspreid over de yohurt. Doe hetzelfde met de granaatappel.
5. Doe honing naar smaakt over de kant waar de granola nog zichtbaar is en genieten maar.

**Opmerking**

Je kan granola kopen in de winkel en kiezen voor een mix naar keuze. Maar granola kan ook zelf gemaakt worden. Ook als je het zelf maakt heb je heel veel vrijheid in wat je erin doet. Mocht je een basisreceptje willen,  kan je die hier ook terugvinden!