+++
CookingTime = 50
authors = [ "Jan en Femke" ]
date = 2020-11-04T08:53:31Z
exerpt = "Een lekkere dessertje het hele jaar door! "
hero = "/uploads/img_20201026_205803-2.jpg"
title = "Luchtige appelcake "

+++
# Voor 8 personen 

## Ingrediënten 

* 250 gram kristalsuiker 
* 1  zakje vanillesuiker
* 250 gram boter 
* 250 gram zelfrijzende bloem 
* 4 eieren 
* 4/5 appels 
* Kaneel (optioneel) 

## Bereidingswijze 

 1. Verwarm de oven voor op 170 graden. 
 2. Meng de kritstalsuiker en de zachte boter totdat het een soort pasta is. Doe hier ook vanillesuiker bij.  
 3. Splits de eieren en doe één voor één de eierdooiers bij de suiker en boter. 
 4. Zeef de zelfrijzende bloem beetje bij beetje bij het beslag. 
 5. Klop het eiwit op en eens stijf voeg toe aan de rest van het beslag. 
 6. Voeg eventueel kaneel naar smaak toe. 
 7. Schil de appels en snijd in kleine stukjes. Meng het met een lepel voorzichting onder het mengsel. 
 8. Vet de cakevorm in en voeg een kleine laag bloem toe, zo zal je cake niet plakken. 
 9. Steek de cake in de oven voor zeker 20 minuten. Test met een vork in het midden of de cake goed is. Is dat niet, zet de cake dan langer in. 

